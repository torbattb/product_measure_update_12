import logging
from odoo import api, exceptions, fields, models, tools, SUPERUSER_ID, _
from odoo.exceptions import UserError, AccessError, ValidationError
from odoo.tools.safe_eval import safe_eval
from odoo import api, fields, models, _
from odoo.addons import decimal_precision as dp
from odoo.exceptions import UserError, ValidationError
from odoo.tools import float_utils, float_compare
from datetime import datetime
import xlwt, xlrd, time, os, base64
from odoo.tools.float_utils import float_round
try:
    from StringIO import StringIO, BytesIO
except ImportError:
    from io import StringIO, BytesIO
_logger = logging.getLogger(__name__)
class ProductUomUpdate(models.TransientModel):
    _name = 'base.product.merge.uom.wizard'
    uom_id_from = fields.Many2many('product.product','product_produtc_uom_rel','product_id', 'uom_id','products with unit of measure from', help="Default unit of measure used for all stock operation.")
    uom_id_to = fields.Many2one('uom.uom', 'Unit of Measure To', help="Default unit of measure used for all stock operation.")
    result = fields.Text('Result')
    product_id = fields.Many2one('product.product', 'Product')
    uom_id = fields.Many2one('uom.uom', 'Uom')
    def unit_measure_update(self, context=None):
        product_ids_validate = []
        product_ids_unvalidate = []
        product_ids_validate_name = []
        string_result = ''
        product_model = self.env['product.product']
        new_unit = self.uom_id_to.id
        product_ids = []
        for product in self.uom_id_from:
            product_ids.append(product.id)
        if len(product_ids) > 0:
            context.update({'active_ids': product_ids})
            dst_product_ids = product_model.browse(product_ids)
            uom_factor_dst = self.env['uom.uom'].browse(self.uom_id_to.id).\
                factor
            for dst_product_id in dst_product_ids:
                product_ids_validate_templ = []
                if dst_product_id.uom_id.factor == uom_factor_dst:
                    product_ids_validate.append(dst_product_id.id)
                    product_ids_validate_templ.append(dst_product_id.product_tmpl_id.id)
                    product_ids_validate_name.append(dst_product_id.name)
                    
                    if len(product_ids_validate_templ) > 0:
                        product_ids_tuple = tuple(product_ids_validate_templ)
                        query = '''UPDATE "product_template" SET uom_id = %s , uom_po_id = %s WHERE id IN %%s''' % (new_unit, new_unit)
                        self.env.cr.execute(query, (product_ids_tuple,))
                    if len(product_ids_validate) > 0:
                        if_purchase_order_line = self.env['ir.model'].search([('model', '=', 'purchase.order.line')])
                        if len(if_purchase_order_line) > 0:
                            product_ids_tuple2 = tuple(product_ids_validate)
                            query2 = '''UPDATE "purchase_order_line" SET product_uom = %s WHERE product_id IN %%s''' % (new_unit)
                            self.env.cr.execute(query2, (product_ids_tuple2,))
                    if len(product_ids_validate) > 0:
                        if_purchase_requisition_line = self.env['ir.model'].search([('model', '=', 'purchase.requisition.line')])
                        if len(if_purchase_requisition_line) > 0:
                            product_ids_tuple8 = tuple(product_ids_validate)
                            query8 = '''UPDATE "purchase_requisition_line" SET product_uom_id = %s WHERE product_id IN %%s''' % (new_unit)
                            self.env.cr.execute(query8, (product_ids_tuple8,))
                    if len(product_ids_validate) > 0:
                        if_sale_order_line = self.env['ir.model'].search([('model', '=', 'sale.order.line')])
                        if len(if_sale_order_line) > 0:
                            product_ids_tuple3 = tuple(product_ids_validate)
                            query3 = '''UPDATE "sale_order_line" SET product_uom = %s WHERE product_id IN %%s''' % (new_unit)
                            self.env.cr.execute(query3, (product_ids_tuple3,))
                    if len(product_ids_validate) > 0:
                        product_ids_tuple4 = tuple(product_ids_validate)
                        query4 = '''UPDATE "stock_move" SET product_uom = %s WHERE product_id IN %%s''' % (new_unit)
                        self.env.cr.execute(query4, (product_ids_tuple4,))
                    if len(product_ids_validate) > 0:
                        product_ids_tuple5 = tuple(product_ids_validate)
                        query5 = '''UPDATE "stock_move_line" SET product_uom_id = %s WHERE product_id IN %%s''' % (new_unit)
                        self.env.cr.execute(query5, (product_ids_tuple5,))
                    if len(product_ids_validate) > 0:
                        product_ids_tuple6 = tuple(product_ids_validate)
                        query6 = '''UPDATE "stock_inventory_line" SET product_uom_id = %s WHERE product_id IN %%s''' % (new_unit)
                        self.env.cr.execute(query6, (product_ids_tuple6,))
                else:
                    product_ids_unvalidate.append(dst_product_id.name)
                    product_ids_validate.append(dst_product_id.id)
                    product_ids_tmpl_validate.append(dst_product_id.product_tmpl_id.id)
            if len(product_ids_validate) > 0:
                string_aux1 = '\n'.join(product_ids_validate_name)
                string_result += '''Products changed:\n\n %s ''' % (string_aux1)
            if len(product_ids_unvalidate) > 0:
                string_aux2 = '\n'.join(product_ids_unvalidate)
                string_result += '''\n\nProducts that not change because the '''\
                    '''conversion factor is not equal:\n\n %s''' % (string_aux2)
        else:
            string_result = 'Warning! \n\n You must choose at least one record..'
        __, xml_id = self.env['ir.model.data'].get_object_reference('product_uom_update_12',
            'base_product_uom_merge_wizard_result')
        context.update({'default_result': string_result})
        return {
            'res_model': 'base.product.merge.uom.wizard',
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': xml_id,
            'context': context,
            'type': 'ir.actions.act_window',
            'target': 'new',
        }
    def close_vw(self,context=None):
        return self.env['base.product.merge.automatic.wizard'].\
            close_cb(context=context)
class ProductWizard(models.TransientModel):
    _name = 'product.uom.wizard'
    product_id = fields.Many2one('product.product', 'Product')
    wizard_id = fields.Many2one('base.product.merge.uom.wizard')
